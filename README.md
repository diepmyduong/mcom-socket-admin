# Mcom Socket Admin

## Introduction

> Socket Admin SDK for Nodejs Server

## Code Samples

```javascript
var { AdminSocket }= require('mcom-socket-admin');

const admin = new AdminSocket('my-socket-server-secret', {
    hostname: '192.168.1.43:8000',
});

const interval = setInterval(() => {
    if(admin.isConnected) {
        const userChannels = ['msg'];
        const publicChannels = ['news'];    
        admin.registerConnection('user1',userChannels, publicChannels).then(token => {
             // token is the connection string for client login to socker server. Client is only allowed subscribe to userChannels and publicChannels
        });
        clearInterval(interval);
        setInterval(() => {
            console.log('push msg');
            admin.pushUserChannel('user1', 'msg', 'you have a msg');
        }, 3000);
        setInterval(() => {
            console.log('push news');
            admin.pushPublicChannel('news', 'New Newssss');
        }, 5000);
    }
}, 3000);



```

## Installation

```
npm i mcom-socket-admin --save
```