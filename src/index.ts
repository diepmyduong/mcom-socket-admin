import * as socketClient from 'socketcluster-client';
import { ISocketOption } from './schema';

export class AdminSocket {
  public option = {
    autoReconnectOptions: {
      initialDelay: 10000, // milliseconds
      maxDelay: 60000, // milliseconds
      multiplier: 1.5, // decimal
      randomness: 10000, // milliseconds
    },
  };
  public secret: string;
  public isConnecting: boolean = false;
  public isConnected: boolean = false;
  public socket: any;
  private logger: any;
  constructor(secret: string, option: ISocketOption) {
    this.option = Object.assign(this.option, {
      hostname: option.hostname,
    });
    this.logger = option.logger || console.log;
    this.secret = secret;
    this.connect();
  }

  public connect(option?: any) {
    if (this.isConnecting) {
      return;
    }
    if (this.socket) {
      this.socket.disconnect();
      this.socket = undefined;
    }
    this.socket = socketClient.create(option || this.option);
    this.isConnecting = true;
    this.socket.on('connect', () => {
      this.logger('Admin socket connected to socket server');
      this.logger('Admin logining to socket server...');
      this.socket.emit('adminLogin', { secret: this.secret }, (err: any) => {
        if (!err) {
          this.logger('Admin socket logined!!!');
          this.isConnected = true;
        } else {
          this.isConnected = false;
        }
        this.isConnecting = false;

      });
    });
    this.socket.on('disconnect', () => {
      this.logger('Admin socket disconnected');
    });
    this.socket.on('error', (err: Error) => {
      this.logger('Admin socket has error', err.message);
    });
  }

  public registerConnection(userId: string, userChannels: string[] = [], publicChannels: string[] = []) {
    return new Promise((resolve, reject) => {
      this.socket.emit(
        'regisConnection',
        {
          subs: [...userChannels.map(c => [userId, c].join('.')), ...publicChannels],
        },
        (err: any, token: string) => {
          if (err) {
            this.connect();
            reject(err);
          } else {
            resolve(token);
          }
        },
      );
    });
  }

  public pushUserChannel(userId: string, channel: string, message: any) {
    this.socket.publish([userId, channel].join('.'), message);
  }
  public pushPublicChannel(channel: string, message: any) {
    this.socket.publish(channel, message);
  }
}
