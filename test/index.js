var { AdminSocket }= require('../lib');

const admin = new AdminSocket('my-socket-server-secret', {
    hostname: '192.168.1.43:8000',
});

const interval = setInterval(() => {
    if(admin.isConnected) {
        admin.registerConnection('user1', ['msg'], ['news']).then(console.log);
        clearInterval(interval);
        setInterval(() => {
            console.log('push msg');
            admin.pushUserChannel('user1', 'msg', 'you have a msg');
        }, 3000);
        setInterval(() => {
            console.log('push news');
            admin.pushPublicChannel('news', 'New Newssss');
        }, 5000);
    }
}, 3000);


